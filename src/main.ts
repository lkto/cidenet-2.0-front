import { createApp } from 'vue'
import App from './App.vue'
import * as $ from 'jquery';
import dt from 'datatables.net'

declare global {
    interface Window { $: any; dt: any }
}
window.$ = require('jquery')
window.dt = require('datatables.net')

createApp(App).mount('#app')
